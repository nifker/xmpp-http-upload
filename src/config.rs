use qstring::QString;
use serde::*;
use std::error::Error;
use std::path::{Path, PathBuf};

#[derive(Deserialize)]
pub struct Config {
	pub server: ServerConfig,
	//None if nothing should be logged
	pub log: Option<LogConfig>,
	#[serde(default)]
	pub daemon: Option<DaemonConfig>,
	pub database: DatabaseConfig,
}

#[derive(Deserialize)]
pub enum DatabaseConfig {
	#[serde(rename = "file")]
	File(FileDatabaseConfig),
	#[cfg(any(feature = "postgres"))]
	#[serde(rename = "server")]
	Server(ServerDatabaseConfig),
}

// SQLite / LocalFileDatabase
#[derive(Deserialize)]
pub struct FileDatabaseConfig {
	//directory for the database file and user uploaded files
	pub path: PathBuf,
	pub filename: PathBuf,
}

// (SQL) Server connections
#[derive(Deserialize)]
#[cfg(any(feature = "postgres"))]
pub struct ServerDatabaseConfig {
	pub address: std::net::IpAddr,
	pub port: u16,
	pub username: String,
	pub password: String,
	pub database: String,
	pub server_type: ServerDatabaseType,
}

#[derive(Clone, Copy, Deserialize, Debug)]
pub enum ServerDatabaseType {
	PostgreSql,
}

#[derive(Deserialize)]
pub struct LogConfig {
	pub file: PathBuf,
}

#[derive(Deserialize)]
pub struct ServerConfig {
	//listening ip address
	pub address: std::net::IpAddr,
	//Port on which the socket is running - 1024-65535 - this port shall not be forwarded
	pub port: u16,
	//A secret string which is used to authenticate the uploads
	pub secret_string: String,
	//MIME-types which are disallowed from being uploaded
	#[serde(default)]
	mime_blacklist: Vec<mime_serde_shim::Wrapper>,
	//time in seconds after which a file is removed
	pub expiration_time: Option<u64>,
	//lockfile
	pub lock_file: PathBuf,
}

#[derive(Deserialize)]
pub struct DaemonConfig {
	// PID file for the daemon
	pub pid_file: PathBuf,
	// User to be used for the daemon
	pub user: String,
	// redirect stdout
	pub stdout: Option<PathBuf>,
	// redirect stderr
	pub stderr: Option<PathBuf>,
}

impl Config {
	pub fn default<P: AsRef<Path> + ?Sized>(file: &P) -> Result<Self, Box<dyn Error>> {
		let data = std::fs::read_to_string(file)?;
		Ok(toml::from_str(&data)?)
	}

	pub fn is_mime_blacklisted(&self, mime: &mime::Mime) -> bool {
		for m in self.server.mime_blacklist.iter() {
			if *m == *mime {
				return true;
			}
		}
		false
	}
}

#[derive(Clone, PartialEq)]
pub enum HttpUploadVersion {
	V1,
	V2,
}

impl HttpUploadVersion {
	pub fn as_str(&self) -> &str {
		match self {
			HttpUploadVersion::V1 => "v",
			HttpUploadVersion::V2 => "v2",
		}
	}

	// Tried to get the version from the string and tries to use self as the preferred option
	pub fn determine_from_querystring(qs: &QString) -> Option<Self> {
		if qs.has(HttpUploadVersion::V2.as_str()) {
			Some(HttpUploadVersion::V2)
		}
		else if qs.has(HttpUploadVersion::V1.as_str()) {
			Some(HttpUploadVersion::V1)
		}
		else {
			None
		}
	}
}
