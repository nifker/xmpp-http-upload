use crate::args::*;
use crate::config::*;
use crate::database::*;
use crate::overwatch::*;
use crate::*;
use daemonize::Daemonize;
use lockfile::Lockfile;
use loglite::*;
use signal_hook::consts::{SIGINT, SIGTERM};
use std::error::Error;
use std::fs::File;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;
use tiny_http::*;

pub struct XmppUploadServer {
	log: Log,
	database: Arc<dyn Database>,
	config: Arc<Config>,
	exit: Arc<AtomicBool>,
	_lockfile: Arc<Lockfile>,
	server: Server,
}

impl XmppUploadServer {
	pub fn new() -> Result<Self, Box<dyn Error>> {
		let exit = Arc::new(AtomicBool::new(false));
		//Handle POSIX interrupts
		signal_hook::flag::register(SIGTERM, Arc::clone(&exit))?;
		signal_hook::flag::register(SIGINT, Arc::clone(&exit))?;

		let config_file = match parse_cli_args() {
			Some(c) => c,
			None => return Err(usage_error("Missing config file")),
		};

		let config = Arc::new(Config::default(&config_file)?);

		match &config.daemon {
			Some(daemon_cfg) => {
				let mut daemonize = Daemonize::new()
					.pid_file(&daemon_cfg.pid_file)
					.chown_pid_file(true)
					.working_directory("/tmp")
					.user(&*daemon_cfg.user)
					.group(&*daemon_cfg.user)
					.umask(0o774);

				if let Some(stdout_path) = &daemon_cfg.stdout {
					daemonize = daemonize.stdout(File::create(stdout_path)?) // Redirect stdout
				}

				if let Some(stderr_path) = &daemon_cfg.stderr {
					daemonize = daemonize.stderr(File::create(stderr_path)?) // Redirect stderr
				}

				daemonize.start()?;
			}
			None => {}
		}

		let log = {
			match config.log {
				Some(ref log) => Log::new(log.file.as_path(), true)
					.inspect_err(|e| eprintln!("{}", e))
					.unwrap_or_default(),
				None => Log::new_void(),
			}
		};

		let lockfile = Lockfile::create(config.server.lock_file.as_path())?;

		let database = DatabaseHelper::create_from_config(&config.database)?;

		let server = match Server::http(format!("{}:{}", config.server.address, config.server.port))
		{
			Ok(s) => s,
			Err(e) => return Err(e),
		};

		Ok(Self {
			log,
			database,
			config,
			exit,
			_lockfile: Arc::new(lockfile),
			server,
		})
	}

	pub fn run(&self) -> () {
		if let Some(exp_time) = self.config.server.expiration_time {
			//create thread and handle all saved files and remove them when expiry time is reached
			overwatch_handler(
				self.database.clone(),
				exp_time,
				self.log.clone(),
				self.exit.clone(),
			);
		}

		loop {
			if self.exit.load(Ordering::Relaxed) {
				println!("Received interrupt, exiting server");
				break;
			}

			match self.server.recv_timeout(Duration::from_millis(500)) {
				Ok(Some(mut request)) => {
					dbg!(
						"received request!",
						request.method(),
						request.url(),
						request.headers()
					);

					let mut response = request::process_request(
						&mut request,
						&self.config,
						&self.log,
						self.database.clone(),
					);
					response.add_header(
						Header::from_bytes(
							http::header::SERVER.as_str().as_bytes(),
							env!("CARGO_PKG_NAME").as_bytes(),
						)
						.unwrap(),
					);
					request.respond(response).ok();
				}
				Ok(_) => {}
				Err(_) => {}
			}
		}
	}
}

impl Drop for XmppUploadServer {
	fn drop(&mut self) {
		if let Some(daemon) = &self.config.daemon {
			std::fs::remove_file(&daemon.pid_file).ok();
		}
	}
}
