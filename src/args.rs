use getargs::{Arg, Options};
use std::env::args;
use std::error::Error;
use std::path::{Path, PathBuf};

const USAGE_HELP_MESSAGE: &'static str = r"Usage: xmpp-http-upload [ARGS]
Starts the daemon for the external xmpp-http-upload server.

-c, --config [FILE]		path to the corresponding config file
-v, --version			displays the current version and exits";

pub fn parse_cli_args() -> Option<PathBuf> {
	let args = args().skip(1).collect::<Vec<_>>();
	let mut opts = Options::new(args.iter().map(String::as_str));

	let mut config_file = None;

	while let Some(arg) = opts.next_arg().expect("argument parsing error") {
		match arg {
			Arg::Short('h') | Arg::Long("help") => {
				eprintln!("{}", USAGE_HELP_MESSAGE);
				std::process::exit(0);
			}
			Arg::Short('v') | Arg::Long("version") => {
				println!("{}", env!("CARGO_PKG_VERSION"));
				std::process::exit(0);
			}
			Arg::Short('c') | Arg::Long("config") => {
				config_file = Some(Path::new(&opts.value().unwrap_or("")).to_path_buf())
			}
			_ => {}
		}
	}

	return config_file;
}

pub fn usage_error(error_message: &str) -> Box<dyn Error> {
	format!("{}! See Usage, 'xmpp-http-upload --help'", error_message).into()
}
