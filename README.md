## About
A light external HTTP upload server for XMPP user upload, compatible with prosody and ejabberd configurations.
It currently covers the following features:
- Block mime types
- Remove files when they exceed a time delta
- Logging traffic
- Saves metadata in a small database (CBOR serialized table)
- Switch between implementation versions
- Run the server as an own daemon


## Prerequisites
A [Python](https://www.python.org/) installation of at least version 3.0.
A [Rust](https://www.rust-lang.org/en-US/) installation of version 1.70 or higher.


## Automatic Installation
This will compile and install automatically system-wide for you, but you need to set the log path to match your configuration in install.py,
otherwise you can still create/remove the directory yourself.

Note:  
If you cross-compile, you need to set the COMPILE\_TARGET with the build target of rustc in the script to use the automatic installation.
See [Cross-Compile](https://github.com/japaric/rust-cross).

Clone the repository using git:
> git clone https://gitlab.com/nyovaya/xmpp-http-upload

Enter the directory
> cd xmpp-http-upload

Run to install:
> sudo ./install.py install

If you want to uninstall:
> sudo ./install.py uninstall


## Starting the service

### Systemd

Start the service:
> systemctl start xmpp-http-upload

Stop the service:
> systemctl stop xmpp-http-upload

### Manually Daemonized
Run as root:
> /usr/bin/xmpp-http-upload -c [CONFIG FILE]

And to kill the process:
> kill prep xmpp-http-upload


## XMPP Setup
This depends on your XMPP server software and should be looked up in the corresponding documentation.

### Prosody
See the following website, how the configuration works in [Prosody](https://modules.prosody.im/mod\_http\_upload\_external.html).


### Ejabberd
Use the following lines in your Ejabberd config file:

```
mod_http_upload:
  put_url: "https://your-upload-url.here"
  external_secret: "secret"
```

## Note
Dont forget to properly remap the url in your reverse proxy, otherwise you might run into 404/403 requests. See e.g. [mod\_proxy](https://redmine.lighttpd.net/projects/lighttpd/wiki/Mod_proxy) under proxy.header.

