## Changelog

###
- Updated dependencies
- Log fallback (when file opening fails)

### 0.5.1
- Added support for PostgreSQL and SQLite
- Updated systemd service file to support daemon
- Small bug fixes

### 0.5.0
- Added HEAD API Test
- Tests will try to get server address, port and secret from the config file under assets/
- Filenames are now hashed serverside and saved by their name
- Filenames are not restricted to the format 'foo/bar.ext' anymore
- Configuration file now requires specificing the database type (File or Server) as preparation for database implementations to come
- Some adjustments to logging output
- Fixed content-length for HEAD requests bigger than 32KiB
- Flag to kill runnning server got removed and the script is asking on cli instead
- Updated dependencies

### 0.4.2
- Removed version from config, version is only determined from the url
- The 'Overwatch' thread is blocking the mutex less, increasing overall performance
- Return more errors when headers are missing
- Added unit tests to test the HTTP API
- Added install flag to kill the currently running server
- Many bug fixes

### 0.4.1
- Added command line arguments to the server (see --help option)
- Rewrote installation script in Python3 and added more commands/flags
- Added ability to enable to run the server as a daemon
- Changes to the config's file structure

### 0.4.0
- Code consilidation
- Migrate from hyper to tiny-http

### 0.3.2
- Changed license
- Update dependencies
