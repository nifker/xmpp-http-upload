mod common;
use crate::common::*;
use std::error::Error;

const VERSION: HttpUploadVersion = HttpUploadVersion::V2;

#[test]
fn file_upload_png() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("png");
	let content = generate_file();
	assert_eq!(
		upload_file(&file_name, &content, VERSION, Some(mime::IMAGE_PNG))?,
		200
	);
	Ok(())
}

#[test]
fn file_upload_conflict() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("png");
	let content = generate_file();
	assert_eq!(
		upload_file(&file_name, &content, VERSION, Some(mime::IMAGE_PNG))?,
		200
	);
	assert_eq!(
		upload_file(&file_name, &content, VERSION, Some(mime::IMAGE_PNG))?,
		409
	);
	Ok(())
}

#[test]
fn file_upload_invalid_secret_key() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("xml");
	let content = generate_file();
	assert_eq!(
		upload_file_wrong_secret(&file_name, &content, VERSION, Some(mime::TEXT_XML))?,
		403
	);
	Ok(())
}

#[test]
fn file_head_check_headers() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("xml");
	let content = generate_file();
	assert_eq!(
		upload_file(&file_name, &content, VERSION, Some(mime::TEXT_XML))?,
		200
	);
	let (code, headers) = get_head_file(&file_name)?;
	assert!(
		headers
			.get(http::header::CONTENT_LENGTH)
			.unwrap()
			.to_str()?
			== &content.len().to_string()
	);
	assert!(
		headers.get(http::header::CONTENT_TYPE).unwrap().to_str()? == mime::TEXT_XML.essence_str()
	);
	assert_eq!(code, 200);
	Ok(())
}

#[test]
fn file_get_check_headers() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("xml");
	let content = generate_file();
	assert_eq!(
		upload_file(&file_name, &content, VERSION, Some(mime::TEXT_XML))?,
		200
	);

	let (code, _, headers) = retrieve_file(&file_name)?;
	assert!(
		headers.get(http::header::CONTENT_TYPE).unwrap().to_str()? == mime::TEXT_XML.essence_str()
	);

	assert!(
		headers.get(http::header::CONTENT_LENGTH).is_some()
			|| headers.get(http::header::TRANSFER_ENCODING).is_some()
	);

	match headers.get(http::header::CONTENT_LENGTH) {
		Some(v) => assert_eq!(v.to_str()?, &content.len().to_string()),
		_ => {}
	}

	//HTTP 1.1 specific
	match headers.get(http::header::TRANSFER_ENCODING) {
		Some(v) => assert_eq!(v.to_str()?, "chunked"),
		_ => {}
	}

	assert_eq!(code, 200);
	Ok(())
}
