mod common;
use crate::common::*;
use std::error::Error;

const VERSION: HttpUploadVersion = HttpUploadVersion::V1;

#[test]
fn file_upload_png() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("png");
	let content = generate_file();
	assert_eq!(upload_file(&file_name, &content, VERSION, None)?, 200);
	Ok(())
}

#[test]
fn file_upload_conflict() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("png");
	let content = generate_file();
	assert_eq!(upload_file(&file_name, &content, VERSION, None)?, 200);
	assert_eq!(upload_file(&file_name, &content, VERSION, None)?, 409);
	Ok(())
}

#[test]
fn file_upload_invalid_secret_key() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("xml");
	let content = generate_file();
	assert_eq!(
		upload_file_wrong_secret(&file_name, &content, VERSION, None)?,
		403
	);
	Ok(())
}

#[test]
fn file_upload_hex_key_odd_length() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("7z");
	let content = generate_file();
	assert_eq!(
		upload_file_key_odd_length(&file_name, &content, VERSION, None)?,
		400
	);
	Ok(())
}

#[test]
fn file_upload_hex_key_non256bit() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("7z");
	let content = generate_file();
	assert_eq!(
		upload_file_key_non256bit(&file_name, &content, VERSION, None)?,
		400
	);
	Ok(())
}

#[test]
fn file_upload_wrong_upload_version() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("7z");
	let content = generate_file();
	assert_eq!(
		upload_file_key_wrong_upload_version(&file_name, &content, VERSION, None)?,
		400
	);
	Ok(())
}

#[test]
fn file_get_check_headers() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("xml");
	let content = generate_file();
	assert_eq!(upload_file(&file_name, &content, VERSION, None)?, 200);

	let (code, _, headers) = retrieve_file(&file_name)?;
	assert_eq!(code, 200);
	println!("{:?}", headers);
	assert!(
		headers.get(http::header::CONTENT_TYPE).unwrap().to_str()?
			== mime::APPLICATION_OCTET_STREAM.essence_str()
	);

	assert!(
		headers.get(http::header::CONTENT_LENGTH).is_some()
			|| headers.get(http::header::TRANSFER_ENCODING).is_some()
	);

	match headers.get(http::header::CONTENT_LENGTH) {
		Some(v) => assert_eq!(v.to_str()?, &content.len().to_string()),
		_ => {}
	}

	//HTTP 1.1 specific
	match headers.get(http::header::TRANSFER_ENCODING) {
		Some(v) => assert_eq!(v.to_str()?, "chunked"),
		_ => {}
	}

	Ok(())
}

#[test]
fn file_get_with_wrong_filename() -> Result<(), Box<dyn Error>> {
	let file_name = generate_file_name("tar.gz");
	assert_eq!(retrieve_file(&file_name)?.0, 404);
	Ok(())
}
