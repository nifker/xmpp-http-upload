#!/usr/bin/env python3

import sys, os, pwd, subprocess, shutil, getpass, psutil, signal;

def chown_r(fd: str | os.PathLike, uid: int, gid: int):
    # Set owner for the top-level folder
    os.chown(fd, uid, gid)

    for root, dirs, files in os.walk(fd):
        # Set owner on sub-directories
        for momo in dirs:
            os.chown(os.path.join(root, momo), uid, gid)
        # Set owner on files
        for momo in files:
            os.chown(os.path.join(root, momo), uid, gid)


def chmod_r(fd: str | os.PathLike, mode: int):
    # Set perms for the top-level folder
    os.chmod(fd, mode)

    for root, dirs, files in os.walk(fd):
        # Set perms on sub-directories
        for momo in dirs:
            os.chmod(os.path.join(root, momo), mode)
        # Set perms on files
        for momo in files:
            os.chmod(os.path.join(root, momo), mode)

def get_file_owner(filename):
    return pwd.getpwuid(os.stat(filename).st_uid).pw_name

def kill_proc():
    for proc in psutil.process_iter():
        if proc.name() == "xmpp-http-upload":
            proc.send_signal(signal.SIGTERM)
            while proc.is_running():
                pass

USER = "xmpp-external"
SERVICE = "xmpp-http-upload"
SERVICE_FILE = f"/etc/systemd/system/{SERVICE}.service"
CONFIG_PATH = "/etc/" + SERVICE
BIN_PATH = "/usr/bin/" + SERVICE
LOCAL_FILES_PATH = "/var/local/" + SERVICE
COMPILE_TARGET = ""
ERROR_LOG = "build.err"

def compile():
    env = os.environ.copy()
    devnull = os.open("/dev/null", os.O_WRONLY)
    errfile = os.open(ERROR_LOG, os.O_WRONLY | os.O_CREAT)

    cmdbuild = "cargo build --release"
    if COMPILE_TARGET:
        cmdbuild = cmdbuild + " --target=" + COMPILE_TARGET
    print(f"Compiling {SERVICE} binary...")
    if os.geteuid() == 0:
        build = subprocess.run(["su", "-c", cmdbuild, get_file_owner(__file__)], stdout=devnull, stderr=errfile).returncode
    else:
        build = subprocess.run(cmdbuild.split(), stdout=devnull, stderr=errfile).returncode

    if build == 0:
        print("Binary built successfully!")
        os.remove(ERROR_LOG)
    else:
        print("Binary compilation failed!")
        exit(3)

def copy_binary():
    if COMPILE_TARGET:
        shutil.copy(f"target/{COMPILE_TARGET}/release/{SERVICE}", BIN_PATH)
    else:
        shutil.copy(f"target/release/{SERVICE}", BIN_PATH)
    chmod_r(BIN_PATH, 0o755)

if len(sys.argv) < 2:
    print("""Missing argument!
    Usage: ./install.py <OPTION> [ARGS]

    Possible options:
        - install       Systemwide installation
        - uninstall     Removes all data, config and binary files from the system regarding the xmpp-http-upload
        - compile       Only compiles the software and exits

    Possible arguments (except for command 'compile'):
        --systemd       This will also install the systemd service file
        --no-compile    This will not (re)compile the software""")

    exit(1)

systemd = sys.argv.count("--systemd") > 0
no_compile = sys.argv.count("--no-compile") > 0
method = sys.argv[1]

if os.geteuid() != 0 and method != "compile":
    print("Running the script requires root privileges")
    exit(2)

if method == "install":
    if not no_compile:
        compile()

    try:
        pwd.getpwnam(USER)
    except KeyError:
        subprocess.run(["useradd", USER])

    gid = pwd.getpwnam(USER).pw_gid
    uid = -1

    if systemd:
        shutil.copy(f"assets/{SERVICE}.service", SERVICE_FILE)

    if not os.path.exists(CONFIG_PATH):
        os.mkdir(CONFIG_PATH)
    if not os.path.exists(os.path.join(CONFIG_PATH, "config.toml")):
        shutil.copy("assets/config.toml", CONFIG_PATH)
        chown_r(CONFIG_PATH, uid, gid)
        chmod_r(CONFIG_PATH, 0o754)

    try:
        copy_binary()
    except OSError as e:
        if e.errno == 26:
            print("The installed binary is being used. Should the proccess tried to be killed? (y/n)")
            line = sys.stdin.readline().strip()
            if line == "y":
                kill_proc()
                copy_binary()
            else:
                print("Installation aborted")
                exit(3)

    if not os.path.exists(LOCAL_FILES_PATH):
        os.mkdir(LOCAL_FILES_PATH)
        chown_r(LOCAL_FILES_PATH, uid, gid)
        chmod_r(LOCAL_FILES_PATH, 0o774)

    print("Installation successfully completed!")
elif method == "uninstall":
    kill_proc()
    subprocess.run(["userdel", USER])

    # remove systemd service file and reload all service files
    if systemd:
        subprocess.run(["systemctl", "stop", SERVICE])
        subprocess.run(["systemctl", "daemon-reload"])
    if os.path.exists(SERVICE_FILE):
        os.remove(SERVICE_FILE)

    # config file
    shutil.rmtree(CONFIG_PATH)
    # binary executable
    os.remove(BIN_PATH)

    shutil.rmtree(LOCAL_FILES_PATH)

    print("Successfully removed all data!")
elif method == "compile":
    compile()
else:
    print(f"Unknown method '{method}'")
